const get_one_tx={"id":4,"shop_id":9,"shop_user_id":7,"card_id":4,"card_num":"4444444","money":"1.00","money_befor":"0.00","status":1,"ip":"222.87.187.218","pay_aid":1,"pay_date":1571884377,"create_time":"2019-10-24 10:29:16","card":{"id":4,"shop_id":9,"bank_name":"建设银行","bank_num":"4444444","username":"444","type":4,"create_time":"2019-10-23 09:17:49"}}

const get_sale_money={"sale_all":10,"already":2,"ready":1,"ktx_all":7}

const get_card=[{"id":4,"shop_id":9,"bank_name":"建设银行","bank_num":"4444444","username":"444","type":4,"create_time":"2019-10-23 09:17:49"},{"id":5,"shop_id":9,"bank_name":"农业","bank_num":"666666666","username":"66","type":0,"create_time":"2019-10-23 10:51:02"},{"id":8,"shop_id":9,"bank_name":"工商银行某某某支行","bank_num":"123456789","username":"张密码","type":3,"create_time":"2019-10-24 10:01:11"}]

const get_tx_log=[{"id":3,"shop_id":9,"shop_user_id":7,"card_id":4,"card_num":"","money":"1.00","money_befor":"0.00","status":1,"ip":"1.207.135.142","pay_aid":1,"pay_date":1571883814,"create_time":"2019-10-23 14:49:58","card":{"id":4,"shop_id":9,"bank_name":"建设银行","bank_num":"4444444","username":"444","type":4,"create_time":"2019-10-23 09:17:49"}},{"id":4,"shop_id":9,"shop_user_id":7,"card_id":4,"card_num":"4444444","money":"1.00","money_befor":"0.00","status":1,"ip":"222.87.187.218","pay_aid":1,"pay_date":1571884377,"create_time":"2019-10-24 10:29:16","card":{"id":4,"shop_id":9,"bank_name":"建设银行","bank_num":"4444444","username":"444","type":4,"create_time":"2019-10-23 09:17:49"}},{"id":5,"shop_id":9,"shop_user_id":7,"card_id":5,"card_num":"666666666","money":"1.00","money_befor":"0.00","status":0,"ip":"222.87.187.218","pay_aid":0,"pay_date":0,"create_time":"2019-10-24 10:33:25","card":{"id":5,"shop_id":9,"bank_name":"农业","bank_num":"666666666","username":"66","type":0,"create_time":"2019-10-23 10:51:02"}}]

const  count_order={"id":5,"shop_name":"澳强管理员","today":[{"today_num_total":0,"today_money_total":null}],"yesterday":[{"yesterday_num_total":0,"yesterday_money_total":null}],"total":[{"all_num_total":1,"all_money_total":"188.00"}]}

const  order_detail={"id":1,"order_num":'123456789223',"pay_time":"2019-1-8 10:01:20","total":"999",
					"address":[{
						"name":"全文","address":"某某省某某市某某省某某市某某省某某市某某省某某市","tell":"12345678909"
					}]}
					
const  order_pro=[{"title":'这里是商品标题',"price":"99","pic":require('@/imgs/8.jpg'),
                 "guige":[{"name":'产品颜色',"value":'黑色'}],"num":2},
				 {"title":'这里是商品标题',"price":"299","pic":require('@/imgs/8.jpg'),
				                  "guige":[{"name":'产品颜色',"value":'红色'}],"num":1}
		]

const   content={"shop_id":5,"region_id":2,"shop_name":"\u6d4b\u8bd5","shop_description":"","shop_type_id":"","vip_type":0,"aid":0,"shop_group_id":0,"area":"","shop_address":"\u6c47\u91d1\u4e2d\u5fc3","position":null,"shop_state":1,"sort":0,"img_id":0,"hj_imgs":"","other_imgs":"","shop_phone":"18685497757","shop_collect":0,"shop_sales":"0.00","content":"","xinxin":0,"yy_time":"09:00-19:00","chouyong":0,"laiyu_tel":"","first_mian":"0.00","vip_status":0,"license_url":"","user_sfz_url":"","shop_pic_url":"","shop_time":1600067337,"create_time":"2019-09-14","update_time":"2019-09-14 15:08:57","imgs":null,"card":null}

const   get_coupon=[{"id":2,"type":3,"shop_id":1,"name":"aa","goods_ids":"0","region_id":2,"status":1,"is_show":0,"stock":null,"stock_type":1,"full":30,"reduce":22,"start_time":"2019-09-20","end_time":"2019-10-01","day":null,"create_time":"2019-09-30","update_time":"2019-09-30 09:24:02","delete_time":0},{"id":3,"type":3,"shop_id":1,"name":"9","goods_ids":"0","region_id":2,"status":1,"is_show":0,"stock":null,"stock_type":1,"full":0,"reduce":2,"start_time":"2019-09-20","end_time":"2019-10-01","day":null,"create_time":"2019-09-30","update_time":"2019-09-30 09:24:20","delete_time":0}]

const   my_product=[{
		"id": 8,
		"shop_id": 1,
		"category_id": 5,
		"shop_category_id": [
			"0"
		],
		"banner_imgs": "44,43,42",
		"ct": "",
		"content": "圆形透明碗500套",
		"c_imgs": "45,47,46",
		"description": "圆形透明碗500套",
		"goods_name": "圆形透明碗500套",
		"img_id": 44,
		"leixin": 1,
		"market_price": "158.00",
		"price": "134.00",
		"vip_price": "120.00",
		"stock": 88,
		"sales": 0,
		"is_hot": 0,
		"is_new": 0,
		"start_date": "1970年01月01日",
		"end_date": "1970年01月01日",
		"sy_time": "",
		"yuyue": "",
		"guize": "",
		"aid": 0,
		"fx_tc": "20",
		"operator": "",
		"state": 1,
		"points": 0,
		"banner_img": "",
		"sort": 0,
		"is_hotel": 0,
		"is_first": 0,
		"max_people": 0,
		"tag_ids": "",
		"region_id": 2,
		"courier_type": [
			""
		],
		"create_time": "2019-09-12 15:18:57",
		"update_time": "2019-09-12 15:18:57",
		"delete_time": null,
		"imgs": "http://cdn.ewayz.cn:15001/UploadFiles/Store_Files/store/2/Product/show/thumb350_350/ps_1812281051510011637.png"
	},
	{
		"id": 7,
		"shop_id": 1,
		"category_id": 5,
		"shop_category_id": [
			"0"
		],
		"banner_imgs": "38,37",
		"ct": "",
		"content": "澳强方盒300套-TH",
		"c_imgs": "39,40,41",
		"description": "澳强方盒300套-TH",
		"goods_name": "澳强方盒300套-TH",
		"img_id": 38,
		"leixin": 1,
		"market_price": "49.00",
		"price": "29.90",
		"vip_price": "20.00",
		"stock": 44,
		"sales": 0,
		"is_hot": 0,
		"is_new": 0,
		"start_date": "1970年01月01日",
		"end_date": "1970年01月01日",
		"sy_time": "",
		"yuyue": "",
		"guize": "",
		"aid": 0,
		"fx_tc": "10",
		"operator": "",
		"state": 1,
		"points": 0,
		"banner_img": "",
		"sort": 0,
		"is_hotel": 0,
		"is_first": 0,
		"max_people": 0,
		"tag_ids": "",
		"region_id": 2,
		"courier_type": [
			""
		],
		"create_time": "2019-09-12 15:15:19",
		"update_time": "2019-09-12 15:15:19",
		"delete_time": null,
		"imgs": "http://cdn.ewayz.cn:15001/UploadFiles//Store_Files/store/2/Product/show/thumb350_350/ps_1812281029319689367.png"
	},
	{
		"id": 6,
		"shop_id": 1,
		"category_id": 5,
		"shop_category_id": [
			"0"
		],
		"banner_imgs": "30,31,32",
		"ct": "",
		"content": "澳强圆碗200套-TH",
		"c_imgs": "35,34,33,36",
		"description": "澳强圆碗200套-TH",
		"goods_name": "澳强圆碗200套-TH",
		"img_id": 30,
		"leixin": 1,
		"market_price": "199.00",
		"price": "79.90",
		"vip_price": "69.90",
		"stock": 11,
		"sales": 0,
		"is_hot": 0,
		"is_new": 0,
		"start_date": "1970年01月01日",
		"end_date": "1970年01月01日",
		"sy_time": "",
		"yuyue": "",
		"guize": "",
		"aid": 0,
		"fx_tc": "15",
		"operator": "",
		"state": 0,
		"points": 0,
		"banner_img": "",
		"sort": 0,
		"is_hotel": 0,
		"is_first": 0,
		"max_people": 0,
		"tag_ids": "",
		"region_id": 2,
		"courier_type": [
			""
		],
		"create_time": "2019-09-12 15:07:39",
		"update_time": "2019-09-12 15:07:39",
		"delete_time": null,
		"imgs": "http://cdn.ewayz.cn:15001/UploadFiles//Store_Files/store/2/Product/show/thumb350_350/ps_1812281045195001774.png"
	},
	{
		"id": 5,
		"shop_id": 1,
		"category_id": 5,
		"shop_category_id": [
			"0"
		],
		"banner_imgs": "23,24,25",
		"ct": "",
		"content": "澳强圆碗200套-TH",
		"c_imgs": "26,27,28,29",
		"description": "澳强圆碗200套-TH",
		"goods_name": "澳强圆碗200套-TH",
		"img_id": 24,
		"leixin": 1,
		"market_price": "78.00",
		"price": "29.90",
		"vip_price": "29.00",
		"stock": 77,
		"sales": 0,
		"is_hot": 0,
		"is_new": 0,
		"start_date": "1970年01月01日",
		"end_date": "1970年01月01日",
		"sy_time": "",
		"yuyue": "",
		"guize": "",
		"aid": 0,
		"fx_tc": "3",
		"operator": "",
		"state": 0,
		"points": 0,
		"banner_img": "",
		"sort": 0,
		"is_hotel": 0,
		"is_first": 0,
		"max_people": 0,
		"tag_ids": "",
		"region_id": 2,
		"courier_type": [
			""
		],
		"create_time": "2019-09-12 15:04:15",
		"update_time": "2019-09-12 15:04:15",
		"delete_time": null,
		"imgs":  "http://cdn.ewayz.cn:15001/UploadFiles//Store_Files/store/2/Product/show/thumb350_350/ps_1812281045195001774.png"
	},
	{
		"id": 4,
		"shop_id": 1,
		"category_id": 5,
		"shop_category_id": [
			"0"
		],
		"banner_imgs": "18,19",
		"ct": "",
		"content": "澳强方盒300套-TH",
		"c_imgs": "20,21,22",
		"description": "澳强方盒300套-TH",
		"goods_name": "澳强方盒300套-TH",
		"img_id": 18,
		"leixin": 1,
		"market_price": "228.00",
		"price": "198.00",
		"vip_price": "188.00",
		"stock": 89,
		"sales": 0,
		"is_hot": 0,
		"is_new": 0,
		"start_date": "1970年01月01日",
		"end_date": "1970年01月01日",
		"sy_time": "",
		"yuyue": "",
		"guize": "",
		"aid": 0,
		"fx_tc": "20",
		"operator": "",
		"state": 1,
		"points": 0,
		"banner_img": "",
		"sort": 0,
		"is_hotel": 0,
		"is_first": 0,
		"max_people": 0,
		"tag_ids": "",
		"region_id": 2,
		"courier_type": [
			""
		],
		"create_time": "2019-09-12 14:59:06",
		"update_time": "2019-09-12 14:59:06",
		"delete_time": null,
		"imgs": "http://cdn.ewayz.cn:15001/UploadFiles//Store_Files/store/2/Product/show/thumb350_350/ps_1812281045195001774.png"
	},
	{
		"id": 3,
		"shop_id": 1,
		"category_id": 5,
		"shop_category_id": [
			"0"
		],
		"banner_imgs": "12,13,14",
		"ct": "",
		"content": "圆形透明碗500套",
		"c_imgs": "15,16,17",
		"description": "圆形透明碗500套",
		"goods_name": "圆形透明碗500套",
		"img_id": 12,
		"leixin": 1,
		"market_price": "69.00",
		"price": "59.00",
		"vip_price": "0.01",
		"stock": 97,
		"sales": 2,
		"is_hot": 0,
		"is_new": 0,
		"start_date": "1970年01月01日",
		"end_date": "1970年01月01日",
		"sy_time": "",
		"yuyue": "",
		"guize": "",
		"aid": 0,
		"fx_tc": "10",
		"operator": "",
		"state": 1,
		"points": 0,
		"banner_img": "",
		"sort": 0,
		"is_hotel": 0,
		"is_first": 0,
		"max_people": 0,
		"tag_ids": "",
		"region_id": 2,
		"courier_type": [
			""
		],
		"create_time": "2019-09-12 14:54:02",
		"update_time": "2019-09-29 17:11:46",
		"delete_time": null,
		"imgs": "http://cdn.ewayz.cn:15001/UploadFiles//Store_Files/store/2/Product/show/thumb350_350/ps_1812281051510011637.png"
	}
]

const   get_category=[{"id":21,"shop_id":5,"name":"aaa"},{"id":22,"shop_id":5,"name":"bbb"},{"id":23,"shop_id":5,"name":"ccc"},{"id":24,"shop_id":5,"name":"xxx"},{"id":25,"shop_id":5,"name":"好多个"}]

const   category=[{"category_id":2,"category_name":"美容美体","short_name":"美容美体","pid":0,"level":1,"is_visible":1,"sort":9,"category_pic":"64",
"imgs":require('@/imgs/cate1.jpg')},
	{"category_id":5,"category_name":"瘦身纤体","short_name":"瘦身纤体","pid":0,"level":1,"is_visible":1,"sort":8,"category_pic":"65",
"imgs":require('@/imgs/cate2.jpg')},
	{"category_id":1,"category_name":"瑜伽舞蹈","short_name":"瑜伽舞蹈","pid":0,"level":1,"is_visible":1,"sort":7,"category_pic":"69",
"imgs":require('@/imgs/cate3.jpg')},
	{"category_id":3,"category_name":"休闲娱乐","short_name":"休闲娱乐","pid":0,"level":1,"is_visible":1,"sort":6,"category_pic":"67",
"imgs":require('@/imgs/cate4.jpg')}]

const   yunfei=[{"num":3,"mess":"未有商品使用"},{"num":5,"mess":"未有商品使用"},{"num":7,"mess":"未有商品使用"}]

const   yfmoban={"name":3,"fanwei":"未有商品使用",
			"quyu":[{"province":'辽宁省、吉林省、黑龙江省',"shou":1,"s_price":"0.00","xu":1,"x_price":"2.00"},
			{"province":'贵州省、云南省、广西省',"shou":1,"s_price":"0.00","xu":1,"x_price":"2.00"},
			{"province":'四川省、湖南省、宁夏省',"shou":1,"s_price":"0.00","xu":1,"x_price":"2.00"}]
		}

const   kehu=[{name: 'mingzi',id: 1,pic: require('@/imgs/8.jpg'),tell: '123456789'},
					{name: '名字',id: 2,pic: require('@/imgs/3.jpg'),tell: '123789'}]

const   kh_category=[{"category_id":2,"category_name":"白银用户"},{"category_id":3,"category_name":"黄金用户"},
		{"category_id":4,"category_name":"白金用户"},{"category_id":5,"category_name":"钻石用户"}]

export default {
	count_order,
	order_detail,
	order_pro,
	content,
	get_coupon,
	my_product,
	get_category,
	category,
	yunfei,
	yfmoban,
	kehu,
	kh_category,
	get_tx_log,
	get_sale_money,
	get_card,
	get_one_tx
}
